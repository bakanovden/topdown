// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TopDown/TopDownStateEffect.h"
#include "TopDown/FunctionLibrary/Types.h"
#include "UObject/Interface.h"
#include "TopDownGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTopDownGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TOPDOWN_API ITopDownGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	// UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	// bool AvailableForEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	EPhysicalSurface GetSurfaceType();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	TArray<UTopDownStateEffect*> GetAllCurrentEffects();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	void RemoveEffect(UTopDownStateEffect* RemoveEffect);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	void AddEffect(UTopDownStateEffect* NewEffect);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	void DropWeaponToWorld(FDropItem DropItemInfo);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="TopDownGameActor")
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Count);
};
