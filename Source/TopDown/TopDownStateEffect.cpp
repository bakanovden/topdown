// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownStateEffect.h"

#include "Character/TopDownHealthComponent.h"
#include "Interfaces/TopDownGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

bool UTopDownStateEffect::InitObject(AActor* NewTarget, FName NameBoneHit)
{
    Target = NewTarget;
    NameBone = NameBoneHit;
    ITopDownGameActor::Execute_AddEffect(Target, this);

    return true;
}

void UTopDownStateEffect::DestroyObject()
{
    ITopDownGameActor::Execute_RemoveEffect(Target, this);
    Target = nullptr;

    if (this && this->IsValidLowLevel())
    {
        ConditionalBeginDestroy();
    }
}

bool UTopDownStateEffect_ExecuteOnce::InitObject(AActor* NewTarget, FName NameBoneHit)
{
    Super::InitObject(NewTarget, NameBoneHit);
    ExecuteOnce();
    return true;
}

void UTopDownStateEffect_ExecuteOnce::DestroyObject()
{
    Super::DestroyObject();
}

void UTopDownStateEffect_ExecuteOnce::ExecuteOnce()
{
    if (Target)
    {
        UTopDownHealthComponent* HealthComponent = Cast<UTopDownHealthComponent>(
            Target->GetComponentByClass(UTopDownHealthComponent::StaticClass()));
        if (HealthComponent && HealthComponent->GetIsAlive())
        {
            HealthComponent->Server_ChangeHealthValue(Power);
        }
    }
    DestroyObject();
}

bool UTopDownStateEffect_ExecuteTimer::InitObject(AActor* NewTarget, FName NameBoneHit)
{
    Super::InitObject(NewTarget, NameBoneHit);
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(ExecuteTimerHandle, this,
                                               &UTopDownStateEffect_ExecuteTimer::DestroyObject,
                                               Timer, false);
        GetWorld()->GetTimerManager().SetTimer(EffectTimerHandle, this,
                                               &UTopDownStateEffect_ExecuteTimer::Execute,
                                               RateTime, true);
    }

    return true;
}

void UTopDownStateEffect_ExecuteTimer::DestroyObject()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
    }

    Super::DestroyObject();
}

void UTopDownStateEffect_ExecuteTimer::Execute()
{
    if (Target)
    {
        UTopDownHealthComponent* HealthComponent = Cast<UTopDownHealthComponent>(
            Target->GetComponentByClass(UTopDownHealthComponent::StaticClass()));
        if (HealthComponent && HealthComponent->GetIsAlive())
        {
            HealthComponent->Server_ChangeHealthValue(Power);
        }
    }
}

void UTopDownStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTopDownStateEffect, NameBone);
}
