// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define EPS_Metal EPhysicalSurface::SurfaceType1
#define TRACE_CHANNEL_PROJECTILE TraceTypeQuery3

DECLARE_LOG_CATEGORY_EXTERN(LogTopDown, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTopDown_Net, Log, All);
