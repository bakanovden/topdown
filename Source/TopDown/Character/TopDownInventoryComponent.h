// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TopDown/FunctionLibrary/Types.h"

#include "TopDownInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Count);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOPDOWN_API UTopDownInventoryComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UTopDownInventoryComponent();

    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnSwitchWeapon OnSwitchWeapon;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnAmmoChange OnAmmoChange;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnUpdateWeaponSlots OnUpdateWeaponSlots;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnWeaponNotHaveRound OnWeaponNotHaveRound;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
    FOnWeaponHaveRound OnWeaponHaveRound;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
    TArray<FWeaponSlot> WeaponSlots;
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
    TArray<FAmmoSlot> AmmoSlots;

    int32 MaxSlotsWeapon = 0;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType,
                               FActorComponentTickFunction* ThisTickFunction) override;

    bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward = true);
    bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviousIndex, FAdditionalWeaponInfo PreviousWeaponInfo);

    void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);

    FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);
    int32 GetWeaponIndexSlotByName(FName IdWeaponName);
    FName GetWeaponNameBySlotIndex(int32 IndexSlot);
    bool GetWeaponTypeBySlotIndex(int32 IndexSlot, EWeaponType& WeaponType);
    bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType);

    UFUNCTION(BlueprintCallable)
    void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CountChangeAmmo);

    bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmForWeapon);

    //Interface PickUp Actors
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeAmmo(EWeaponType AmmoType);
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeWeapon(int32& FreeSlot);
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar,
                                 FDropItem& DropItemInfo);
    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void Server_TryGetWeaponToInventory(AActor* PickUpActor, FWeaponSlot NewWeapon);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void Server_DropWeaponByIndex(int32 IndexSlot);

    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo);

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FWeaponSlot> GetWeaponSlots();

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FAmmoSlot> GetAmmoSlots();

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
    void Server_InitInventory(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_AmmoChangeEvent(EWeaponType TypeWeapon, int32 Count);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
    void Server_SwitchWeaponEvent(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_WeaponAdditionalInfoChangeEvent(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_WeaponAmmoEmptyEvent(EWeaponType WeaponType);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_WeaponAmmoAvailableEvent(EWeaponType WeaponType);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_UpdateWeaponSlotsEvent(int32 IndexSlotChange, FWeaponSlot NewInfo);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_WeaponNotHaveRoundEvent(int32 IndexSlotWeapon);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void Multicast_WeaponHaveRoundEvent(int32 IndexSlotWeapon);
};
