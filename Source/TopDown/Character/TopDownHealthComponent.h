// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TopDownHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
	
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWN_API UTopDownHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTopDownHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnHealthChange OnHealthChange;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	float Health = 100.0f;

	UPROPERTY(Replicated)
	bool bIsAlive = true;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	float CoefDamage = 1.0f;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	bool GetIsAlive();
	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float NewHealth);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void Server_ChangeHealthValue(float ChangeValue);
	UFUNCTION(BlueprintNativeEvent)
	void DeadEvent();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Health")
	void Multicast_HealthChangeEvent(float CurrentHealth, float Damage);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Health")
	void Multicast_DeadEvent();
		
};
