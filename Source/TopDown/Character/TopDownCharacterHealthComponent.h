// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TopDown/Character/TopDownHealthComponent.h"
#include "TopDownCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

/**
 * 
 */
UCLASS()
class TOPDOWN_API UTopDownCharacterHealthComponent : public UTopDownHealthComponent
{
    GENERATED_BODY()

public:
    UTopDownCharacterHealthComponent();

    UPROPERTY()
    FTimerHandle CooldownShieldTimerHandle;
    FTimerHandle ShieldRecoveryTimerHandle;
    UPROPERTY(BlueprintAssignable)
    FOnShieldChange OnShieldChange;

protected:
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category="Shield")
    float Shield = 100.0f;
  
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
    float ShieldRecoverValue = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
    float ShieldRecoverRate = 0.1f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Shield")
    float CooldownShieldRecoverTime = 5.0f;

    virtual void Server_ChangeHealthValue(float ChangeValue) override;

    UFUNCTION(BlueprintCallable)
    float GetCurrentShield();

    UFUNCTION(BlueprintCallable, Category = "Shield")
    void ChangeShieldValue(float ChangeValue);

    UFUNCTION()
    void CooldownShieldEnd();

    UFUNCTION()
    void RecoveryShield();

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Shield")
    void Multicast_ShieldChangeEvent(float CurrentShield, float Damage);
};
