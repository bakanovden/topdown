// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownCharacter.h"

#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "TopDownInventoryComponent.h"
#include "TopDownCharacterHealthComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TopDown/TopDownGameInstance.h"
#include "Components/InputComponent.h"
#include "Net/UnrealNetwork.h"
#include "TopDown/Components/TopDownStateEffectComponent.h"

ATopDownCharacter::ATopDownCharacter()
{
    GetCapsuleComponent()->InitCapsuleSize(42.f, 90.0f);

    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
    GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
    GetCharacterMovement()->bConstrainToPlane = true;
    GetCharacterMovement()->bSnapToPlaneAtStart = true;

    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
    CameraBoom->TargetArmLength = CameraArmLength;
    CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

    InventoryComponent = CreateDefaultSubobject<UTopDownInventoryComponent>(TEXT("InventoryComponent"));
    HealthComponent = CreateDefaultSubobject<UTopDownCharacterHealthComponent>(TEXT("HealthComponent"));
    StateEffectComponent = CreateDefaultSubobject<UTopDownStateEffectComponent>(TEXT("StateEffects"));

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;

    //Network
    bReplicates = true;
}

void ATopDownCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (CursorToWorld)
    {
        const APlayerController* PC = Cast<APlayerController>(GetController());
        if (PC && PC->IsLocalPlayerController())
        {
            FHitResult TraceHitResult;
            PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
            const FVector CursorFV = TraceHitResult.ImpactNormal;
            const FRotator CursorR = CursorFV.Rotation();
            CursorToWorld->SetWorldLocation(TraceHitResult.Location);
            CursorToWorld->SetWorldRotation(CursorR);
        }
    }

    MovementTick(DeltaSeconds);
}

void ATopDownCharacter::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
    {
        if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
        {
            CursorToWorld = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize,
                                                                   FVector::ZeroVector);
        }
    }

    if (InventoryComponent)
    {
        InventoryComponent->OnSwitchWeapon.AddUniqueDynamic(this, &ATopDownCharacter::InitWeapon);
    }

    if (HealthComponent)
    {
        HealthComponent->OnDead.AddUniqueDynamic(this, &ATopDownCharacter::CharDead);
    }
}

void ATopDownCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
    Super::SetupPlayerInputComponent(NewInputComponent);

    NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownCharacter::InputAxisX);
    NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownCharacter::InputAxisY);

    NewInputComponent->BindAction(TEXT("ZoomCameraDown"), IE_Pressed, this, &ATopDownCharacter::ZoomCameraDown);
    NewInputComponent->BindAction(TEXT("ZoomCameraUp"), IE_Pressed, this, &ATopDownCharacter::ZoomCameraUp);

    NewInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ATopDownCharacter::InputAttackPressed);
    NewInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ATopDownCharacter::InputAttackReleased);

    NewInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &ATopDownCharacter::TryReloadWeapon);

    NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), IE_Pressed, this,
                                  &ATopDownCharacter::TrySwitchNextWeapon);
    NewInputComponent->BindAction(TEXT("SwitchPreviousWeapon"), IE_Pressed, this,
                                  &ATopDownCharacter::TrySwitchPreviousWeapon);

    NewInputComponent->BindAction(TEXT("AbilityAction"), IE_Pressed, this,
                                  &ATopDownCharacter::TryAbilityEnable);

    NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), IE_Pressed, this,
                                  &ATopDownCharacter::DropCurrentWeapon);

    TArray<FKey> HotKeys;
    HotKeys.Add(EKeys::Zero);
    HotKeys.Add(EKeys::One);
    HotKeys.Add(EKeys::Two);
    HotKeys.Add(EKeys::Three);
    HotKeys.Add(EKeys::Four);
    HotKeys.Add(EKeys::Five);
    HotKeys.Add(EKeys::Six);
    HotKeys.Add(EKeys::Seven);
    HotKeys.Add(EKeys::Eight);
    HotKeys.Add(EKeys::Nine);

    NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<9>);
    NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<0>);
    NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<1>);
    NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<2>);
    NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<3>);
    NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<4>);
    NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<5>);
    NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<6>);
    NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<7>);
    NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATopDownCharacter::TKeyPressed<8>);
}

void ATopDownCharacter::InputAxisY(float Value)
{
    AxisY = Value;
}

void ATopDownCharacter::InputAxisX(float Value)
{
    AxisX = Value;
}


void ATopDownCharacter::InputAttackPressed()
{
    if (GetIsAlive())
    {
        AttackCharEvent(true);
    }
}

void ATopDownCharacter::InputAttackReleased()
{
    AttackCharEvent(false);
}

void ATopDownCharacter::AttackCharEvent(bool bIsFiring)
{
    AWeaponDefault* Weapon = GetCurrentWeapon();
    if (Weapon)
    {
        Weapon->Server_SetWeaponStateFire(bIsFiring);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("ATopDownCharacter::AttackCharEvent - CurrentWeapon - NULL"));
    }
}

FVector ATopDownCharacter::GetMousePosition(APlayerController* myController)
{
    FVector EndPoint;
    FVector MouseWorldLocation;
    FVector MouseWorldDirection;
    myController->DeprojectMousePositionToWorld(MouseWorldLocation, MouseWorldDirection);
    float dZ = GetActorLocation().Z - GetDefaultHalfHeight() - MouseWorldLocation.Z;
    // получение растояние от игрока до мышки по Z
    dZ = dZ / MouseWorldDirection.Z; // получаем длину вектора от камеры до игрока по Z
    FHitResult ResultHit;

    EndPoint = MouseWorldLocation + MouseWorldDirection * dZ;
    // получем положение курсора относительно положения игрока по Z чтобы они находились на одном уровне
    GetWorld()->LineTraceSingleByChannel(ResultHit, MouseWorldLocation, EndPoint,
                                         ECC_GameTraceChannel1);

    if (ResultHit.IsValidBlockingHit())
    {
        EndPoint = ResultHit.Location;
    }
    EndPoint.Z = GetActorLocation().Z - GetDefaultHalfHeight();
    return EndPoint;
}

void ATopDownCharacter::MovementTick(float DeltaTime)
{
    if (GetIsAlive())
    {
        if (GetController() && GetController()->IsLocalPlayerController())
        {
            ChangeMovementState();

            FString SEnum = UEnum::GetValueAsString(MovementState);

            if (MovementState == EMovementState::Sprint_Run_State)
            {
                AddMovementInput(GetActorForwardVector(), 1.0f);
            }
            else
            {
                AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
                AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

                APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
                if (myController)
                {
                    const FVector EndPoint = GetMousePosition(myController);

                    const float LookYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), EndPoint).Yaw;

                    SetActorRotation(FQuat(FRotator(0.0f, LookYaw, 0.0f)));
                    Server_SetActorRotationByYaw(LookYaw);
                    if (CurrentWeapon)
                    {
                        FVector Displacement = FVector::ZeroVector;
                        bool bIsReduceDispersion = false;
                        switch (MovementState)
                        {
                        case EMovementState::Aim_State:
                            Displacement = FVector(0.0f, 0.0f, 160.0f);
                            bIsReduceDispersion = true;
                            break;
                        case EMovementState::Aim_Walk_State:
                            Displacement = FVector(0.0f, 0.0f, 160.0f);
                            bIsReduceDispersion = true;
                            break;
                        case EMovementState::Walk_State:
                            Displacement = FVector(0.0f, 0.0f, 120.0f);
                            bIsReduceDispersion = false;
                            break;
                        case EMovementState::Run_State:
                            Displacement = FVector(0.0f, 0.0f, 120.0f);
                            bIsReduceDispersion = false;
                            break;
                        case EMovementState::Sprint_Run_State:
                            break;
                        default:
                            break;
                        }

                        CurrentWeapon->Server_UpdateWeaponByCharacterMovementState(
                            EndPoint + Displacement, bIsReduceDispersion);
                    }
                }
            }
        }
    }
}

void ATopDownCharacter::MovementCameraTick()
{
    if (!UKismetMathLibrary::NearlyEqual_FloatFloat(CameraArmLength, CameraBoom->TargetArmLength))
    {
        CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo_Constant(
            CameraBoom->TargetArmLength,
            CameraArmLength,
            CameraChangeHeightTimerRate,
            CameraChangeZoomDelta
        );
    }
    else
    {
        GetWorldTimerManager().ClearTimer(CameraChangeHeightTimerHandler);
    }
}

void ATopDownCharacter::ZoomCameraDown()
{
    CameraArmLengthUpdate(-1.0f);
}

void ATopDownCharacter::ZoomCameraUp()
{
    CameraArmLengthUpdate(1.0f);
}

int32 ATopDownCharacter::GetCurrentWeaponIndex()
{
    return CurrentIndexWeapon;
}

void ATopDownCharacter::CameraArmLengthUpdate(float Direction)
{
    CameraArmLength += CameraSpeedZoom * Direction;
    CameraArmLength = UKismetMathLibrary::Clamp(CameraArmLength, CameraMinHeight, CameraMaxHeight);
    if (!GetWorldTimerManager().IsTimerActive(CameraChangeHeightTimerHandler))
        GetWorldTimerManager().SetTimer(CameraChangeHeightTimerHandler, this, &ATopDownCharacter::MovementCameraTick,
                                        CameraChangeHeightTimerRate, true, CameraChangeHeightTimerRate);
}

void ATopDownCharacter::CharacterUpdate()
{
    float RunSpeed = MovementInfo.RunSpeed;
    WalkEnabled = false;
    AimEnabled = false;
    SprintRunEnabled = false;
    switch (MovementState)
    {
    case EMovementState::Aim_State:
        RunSpeed = MovementInfo.AimSpeed;
        AimEnabled = true;
        break;
    case EMovementState::Walk_State:
        RunSpeed = MovementInfo.WalkSpeed;
        WalkEnabled = true;
        break;
    case EMovementState::Run_State:
        RunSpeed = MovementInfo.RunSpeed;
        break;
    case EMovementState::Aim_Walk_State:
        RunSpeed = MovementInfo.AimWalkSpeed;
        AimEnabled = true;
        WalkEnabled = true;
        break;
    case EMovementState::Sprint_Run_State:
        RunSpeed = MovementInfo.SprintRunSpeed;
        SprintRunEnabled = true;
        break;
    default:
        break;
    }

    GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
}

void ATopDownCharacter::ChangeMovementState()
{
    EMovementState NewMovementState = EMovementState::Run_State;

    if (SprintRunEnabled)
    {
        WalkEnabled = false;
        AimEnabled = false;
        NewMovementState = EMovementState::Sprint_Run_State;
    }
    else if (WalkEnabled && AimEnabled)
    {
        NewMovementState = EMovementState::Aim_Walk_State;
    }
    else if (AimEnabled)
    {
        NewMovementState = EMovementState::Aim_State;
    }
    else if (WalkEnabled)
    {
        NewMovementState = EMovementState::Walk_State;
    }

    Server_SetMovementState(NewMovementState);

    AWeaponDefault* myWeapon = GetCurrentWeapon();
    if (myWeapon)
    {
        myWeapon->Server_UpdateStateWeapon(NewMovementState);
    }
}


// On Server
void ATopDownCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo AdditionalWeaponInfo,
                                   int32 NewCurrentIndexWeapon)
{
    if (CurrentWeapon)
    {
        CurrentWeapon->Destroy();
        CurrentWeapon = nullptr;
    }

    UTopDownGameInstance* myGameInstance = Cast<UTopDownGameInstance>(GetGameInstance());
    FWeaponInfo myWeaponInfo;
    if (myGameInstance)
    {
        if (myGameInstance->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
        {
            if (myWeaponInfo.WeaponClass)
            {
                FTransform SpawnTransform(FRotator::ZeroRotator, FVector::ZeroVector, FVector::OneVector);

                FActorSpawnParameters SpawnParameters;
                SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                SpawnParameters.Owner = this;
                SpawnParameters.Instigator = GetInstigator();

                AWeaponDefault* myWeapon = Cast<AWeaponDefault>(
                    GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnTransform, SpawnParameters));
                if (myWeapon)
                {
                    FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
                    myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
                    CurrentWeapon = myWeapon;

                    myWeapon->IdWeaponName = IdWeaponName;
                    myWeapon->WeaponSetting = myWeaponInfo;
                    myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
                    myWeapon->Server_UpdateStateWeapon(MovementState);
                    myWeapon->AdditionalWeaponInfo = AdditionalWeaponInfo;
                    CurrentIndexWeapon = NewCurrentIndexWeapon;

                    myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownCharacter::WeaponReloadStart);
                    myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownCharacter::WeaponReloadEnd);
                    myWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownCharacter::WeaponFireStart);

                    if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
                        CurrentWeapon->InitReload();

                    if (InventoryComponent)
                        InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
                }
            }
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("ATopDownCharacter::InitWeapon - Weapon not found in table -NULL"));
        }
    }
}

void ATopDownCharacter::RemoveCurrentWeapon()
{
}

void ATopDownCharacter::Multicast_EnableRagDoll_Implementation()
{
    if (GetMesh())
    {
        GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
        GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
        GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
        GetMesh()->SetSimulatePhysics(true);
    }
}

void ATopDownCharacter::TryReloadWeapon()
{
    Server_TryReloadWeapon();
}

void ATopDownCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
    WeaponReloadStart_BP(Anim);
    Multicast_AnimStart(Anim);
}

void ATopDownCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
    if (InventoryComponent && CurrentWeapon)
    {
        InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
        InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
    }
    if (!bIsSuccess)
    {
        Multicast_AnimMontageStop();
    }

    WeaponReloadEnd_BP(bIsSuccess);
}

void ATopDownCharacter::WeaponFireStart(UAnimMontage* Anim)
{
    if (InventoryComponent && CurrentWeapon)
        InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
    Multicast_AnimStart(Anim);
    WeaponFireStart_BP(Anim);
}

void ATopDownCharacter::Multicast_AnimStart_Implementation(UAnimMontage* Anim)
{
    PlayAnimMontage(Anim);
}

void ATopDownCharacter::Multicast_AnimMontageStop_Implementation()
{
    StopAnimMontage();
}

void ATopDownCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATopDownCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
}

void ATopDownCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATopDownCharacter::CharDead_BP_Implementation()
{
}

void ATopDownCharacter::TrySwitchNextWeapon()
{
    if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
    {
        int8 OldIndex = CurrentIndexWeapon;
        FAdditionalWeaponInfo OldInfo;
        if (CurrentWeapon)
        {
            OldInfo = CurrentWeapon->AdditionalWeaponInfo;
            if (CurrentWeapon->bWeaponReloading)
            {
                CurrentWeapon->CancelReload();
            }
        }

        if (InventoryComponent)
        {
            if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
            {
            }
        }
    }
}

void ATopDownCharacter::TrySwitchPreviousWeapon()
{
    if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
    {
        const int8 OldIndex = CurrentIndexWeapon;
        FAdditionalWeaponInfo OldInfo;
        if (CurrentWeapon)
        {
            OldInfo = CurrentWeapon->AdditionalWeaponInfo;
            if (CurrentWeapon->bWeaponReloading)
            {
                CurrentWeapon->CancelReload();
            }
        }

        if (InventoryComponent)
        {
            if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
            {
            }
        }
    }
}

void ATopDownCharacter::Server_TrySwitchWeaponToIndexByKeyInput_Implementation(int32 IndexWeapon) const
{
    if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(IndexWeapon))
    {
        if (CurrentIndexWeapon != IndexWeapon && InventoryComponent)
        {
            const int32 OldIndex = CurrentIndexWeapon;
            FAdditionalWeaponInfo OldInfo;

            if (CurrentWeapon)
            {
                OldInfo = CurrentWeapon->AdditionalWeaponInfo;
                if (CurrentWeapon->bWeaponReloading)
                    CurrentWeapon->CancelReload();
            }

            InventoryComponent->SwitchWeaponToIndex(IndexWeapon, OldIndex, OldInfo);
        }
    }
}

void ATopDownCharacter::TryAbilityEnable()
{
    if (AbilityEffect)
    {
        UTopDownStateEffect* NewEffect = NewObject<UTopDownStateEffect>(this, AbilityEffect);
        if (NewEffect)
        {
            NewEffect->InitObject(this, NAME_None);
        }
    }
}

void ATopDownCharacter::DropCurrentWeapon()
{
    if (InventoryComponent)
    {
        InventoryComponent->Server_DropWeaponByIndex(CurrentIndexWeapon);
    }
}

void ATopDownCharacter::CharDead()
{
    if (IsLocallyControlled())
    {
        if (GetCursorToWorld())
        {
            GetCursorToWorld()->SetVisibility(false);
        }

        AttackCharEvent(false);
    }

    if (HasAuthority())
    {
        const int32 RndIdx = FMath::RandRange(0, DeadMontages.Num() - 1);
        float AnimLength = 0.0f;
        if (DeadMontages.IsValidIndex(RndIdx) && DeadMontages[RndIdx])
        {
            AnimLength = DeadMontages[RndIdx]->GetPlayLength();
            Multicast_AnimStart(DeadMontages[RndIdx]);
        }

        if (GetController())
        {
            GetController()->UnPossess();
        }

        //Timer ragdoll
        GetWorldTimerManager().SetTimer(RagDollTimerHandle, this, &ATopDownCharacter::Multicast_EnableRagDoll, AnimLength);
        
        SetLifeSpan(20.0f);
        
        if (GetCurrentWeapon())
        {
            GetCurrentWeapon()->SetLifeSpan(20.0f);
        }
    }

    if (GetCapsuleComponent())
    {
        GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
    }

    CharDead_BP();
}

float ATopDownCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                    AActor* DamageCauser)
{
    const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

    if (GetIsAlive())
    {
        HealthComponent->Server_ChangeHealthValue(-ActualDamage);
    }

    if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
    {
        AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
        if (myProjectile)
        {
            UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, Execute_GetSurfaceType(this));
        }
    }
    return ActualDamage;
}

bool ATopDownCharacter::GetIsAlive() const
{
    return HealthComponent && HealthComponent->GetIsAlive();
}

void ATopDownCharacter::Server_SetActorRotationByYaw_Implementation(float Yaw)
{
    Multicast_SetActorRotationByYaw(Yaw);
}

void ATopDownCharacter::Multicast_SetActorRotationByYaw_Implementation(float Yaw)
{
    if (GetController() && !GetController()->IsLocalPlayerController())
    {
        SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
    }
}

void ATopDownCharacter::Server_SetMovementState_Implementation(EMovementState NewMovementState)
{
    MovementState = NewMovementState;
    Multicast_SetMovementState(MovementState);
    FString SEnum = UEnum::GetValueAsString(NewMovementState);
}

void ATopDownCharacter::Multicast_SetMovementState_Implementation(EMovementState NewMovementState)
{
    MovementState = NewMovementState;
    CharacterUpdate();
}

void ATopDownCharacter::Server_TryReloadWeapon_Implementation()
{
    if (GetIsAlive() && CurrentWeapon && !CurrentWeapon->bWeaponReloading)
    {
        if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound
            && CurrentWeapon->CheckCanWeaponReload())
        {
            CurrentWeapon->InitReload();
        }
    }
}

EPhysicalSurface ATopDownCharacter::GetSurfaceType_Implementation()
{
    EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
    if (HealthComponent && HealthComponent->GetCurrentShield() <= 0.0f)
    {
        USkeletalMeshComponent* myMesh = GetMesh();
        if (myMesh)
        {
            UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
            if (myMaterial)
            {
                UPhysicalMaterial* PhysicalMaterial = myMaterial->GetPhysicalMaterial();
                Result = PhysicalMaterial->SurfaceType;
            }
        }
    }
    return Result;
}

TArray<UTopDownStateEffect*> ATopDownCharacter::GetAllCurrentEffects_Implementation()
{
    return StateEffectComponent->GetEffects();
}

void ATopDownCharacter::RemoveEffect_Implementation(UTopDownStateEffect* RemoveEffect)
{
    StateEffectComponent->RemoveEffect(RemoveEffect);
}

void ATopDownCharacter::AddEffect_Implementation(UTopDownStateEffect* NewEffect)
{
    StateEffectComponent->AddEffect(NewEffect);
}

void ATopDownCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATopDownCharacter, MovementState);
    DOREPLIFETIME(ATopDownCharacter, CurrentWeapon);
    DOREPLIFETIME(ATopDownCharacter, CurrentIndexWeapon);
}
