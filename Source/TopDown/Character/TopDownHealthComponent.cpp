// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownHealthComponent.h"

#include "Net/UnrealNetwork.h"

UTopDownHealthComponent::UTopDownHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = true;

    SetIsReplicatedByDefault(true);
}

void UTopDownHealthComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTopDownHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                            FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UTopDownHealthComponent::GetIsAlive()
{
    return bIsAlive;
}

float UTopDownHealthComponent::GetCurrentHealth()
{
    return Health;
}

void UTopDownHealthComponent::SetCurrentHealth(float NewHealth)
{
    Health = NewHealth;
    Multicast_HealthChangeEvent(Health, 0);
}

void UTopDownHealthComponent::Server_ChangeHealthValue_Implementation(float ChangeValue)
{
    if (bIsAlive)
    {
        Health = FMath::Clamp(Health + (ChangeValue * CoefDamage), 0.0f, 100.0f);
        Multicast_HealthChangeEvent(Health, ChangeValue);
        if (Health <= 0.0f)
        {
            bIsAlive = false;
            Multicast_DeadEvent();
        }
    }
}

void UTopDownHealthComponent::Multicast_HealthChangeEvent_Implementation(float CurrentHealth, float Damage)
{
    OnHealthChange.Broadcast(CurrentHealth, Damage);
}

void UTopDownHealthComponent::Multicast_DeadEvent_Implementation()
{
    OnDead.Broadcast();
}

void UTopDownHealthComponent::DeadEvent_Implementation()
{
    // BP
}

void UTopDownHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTopDownHealthComponent, Health);
    DOREPLIFETIME(UTopDownHealthComponent, bIsAlive);
}