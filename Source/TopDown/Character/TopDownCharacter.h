// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDown/Weapons/WeaponDefault.h"
#include "TopDown/FunctionLibrary/Types.h"
#include "TopDown/Interfaces/TopDownGameActor.h"
#include "TopDownCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownCharacter : public ACharacter, public ITopDownGameActor
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
    float CameraArmLength = 800.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
    float CameraMinHeight = 400.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
    float CameraMaxHeight = 800.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
    float CameraSpeedZoom = 50.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
    float CameraChangeZoomDelta = 500.0f;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Movement)
    EMovementState MovementState = EMovementState::Run_State;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    FCharacterSpeed MovementInfo;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    bool SprintRunEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    bool WalkEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    bool AimEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Dead)
    TArray<UAnimMontage*> DeadMontages;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ability)
    TSubclassOf<UTopDownStateEffect> AbilityEffect;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    float AccuracyRight = 2.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor)
    UMaterial* CursorMaterial;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor)
    FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

    UPROPERTY(Replicated)
    AWeaponDefault* CurrentWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Effect)
    FName EffectAttachedSocketName;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Demo)
    FName InitWeaponName;
    UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
    int32 CurrentIndexWeapon = 0;

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* TopDownCameraComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UDecalComponent* CursorToWorld;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UTopDownInventoryComponent* InventoryComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UTopDownCharacterHealthComponent* HealthComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UTopDownStateEffectComponent* StateEffectComponent;

    FTimerHandle CameraChangeHeightTimerHandler;
    float CameraChangeHeightTimerRate = 0.01f;

    FTimerHandle RagDollTimerHandle;

    float AxisX = 0.0f;
    float AxisY = 0.0f;


public:
    ATopDownCharacter();

    virtual void Tick(float DeltaSeconds) override;
    virtual void BeginPlay() override;
    virtual void SetupPlayerInputComponent(UInputComponent* NewInputComponent) override;

    UFUNCTION()
    void InputAxisY(float Value);
    UFUNCTION()
    void InputAxisX(float Value);
    UFUNCTION()
    void MovementTick(float DeltaTime);
    UFUNCTION()
    void MovementCameraTick();

    UFUNCTION()
    void ZoomCameraDown();
    UFUNCTION()
    void ZoomCameraUp();

    UFUNCTION()
    void InputAttackPressed();
    UFUNCTION()
    void InputAttackReleased();

    UFUNCTION(BlueprintCallable)
    FORCEINLINE class UDecalComponent* GetCursorToWorld() const { return CursorToWorld; }
    UFUNCTION(BlueprintCallable)
    FORCEINLINE class AWeaponDefault* GetCurrentWeapon() const { return CurrentWeapon; }
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetCurrentWeaponIndex();

    UFUNCTION()
    void CameraArmLengthUpdate(float Direction);

    UFUNCTION(BlueprintCallable)
    void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentIndexWeapon);
    UFUNCTION(BlueprintCallable)
    void AttackCharEvent(bool bIsFiring);
    UFUNCTION(BlueprintCallable)
    void CharDead();

    UFUNCTION(BlueprintCallable)
    void TryReloadWeapon();
    UFUNCTION()
    void WeaponReloadStart(UAnimMontage* Anim);
    UFUNCTION()
    void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
    UFUNCTION()
    void WeaponFireStart(UAnimMontage* Anim);
    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_AnimStart(UAnimMontage* Anim);
    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_AnimMontageStop();

    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadStart_BP(UAnimMontage* Anim);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadEnd_BP(bool bIsSuccess);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponFireStart_BP(UAnimMontage* Anim);

    UFUNCTION(BlueprintNativeEvent)
    void CharDead_BP();

    void TrySwitchNextWeapon();
    void TrySwitchPreviousWeapon();
    UFUNCTION(Server ,Reliable)
    void Server_TrySwitchWeaponToIndexByKeyInput(int32 IndexWeapon) const;
    void TryAbilityEnable();
    void DropCurrentWeapon();

    template<int32 Id>
    void TKeyPressed()
    {
        Server_TrySwitchWeaponToIndexByKeyInput(Id);
    }

    void RemoveCurrentWeapon();
    FVector GetMousePosition(APlayerController* myController);
    UFUNCTION(BlueprintCallable)
    void CharacterUpdate();
    UFUNCTION(BlueprintCallable)
    void ChangeMovementState();

    UFUNCTION(NetMulticast, Reliable)
    void Multicast_EnableRagDoll();

    FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

    virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent,
                             class AController* EventInstigator, AActor* DamageCauser) override;

    UFUNCTION(BlueprintCallable)
    bool GetIsAlive() const;

    UFUNCTION(Server, Unreliable)
    void Server_SetActorRotationByYaw(float Yaw);

    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_SetActorRotationByYaw(float Yaw);

    UFUNCTION(Server, Reliable)
    void Server_SetMovementState(EMovementState NewMovementState);

    UFUNCTION(NetMulticast, Reliable)
    void Multicast_SetMovementState(EMovementState NewMovementState);

    UFUNCTION(Server, Reliable)
    void Server_TryReloadWeapon();

    //Interface
    virtual EPhysicalSurface GetSurfaceType_Implementation() override;

    virtual TArray<UTopDownStateEffect*> GetAllCurrentEffects_Implementation() override;

    virtual void RemoveEffect_Implementation(UTopDownStateEffect* RemoveEffect) override;

    virtual void AddEffect_Implementation(UTopDownStateEffect* NewEffect) override;
    //end Interface
};
