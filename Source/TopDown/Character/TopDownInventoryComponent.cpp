// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownInventoryComponent.h"

#include "TopDown/TopDownGameInstance.h"
#include "TopDown/Interfaces/TopDownGameActor.h"
#include "Net/UnrealNetwork.h"

UTopDownInventoryComponent::UTopDownInventoryComponent()
{
    PrimaryComponentTick.bCanEverTick = false;

    SetIsReplicatedByDefault(true);
}


void UTopDownInventoryComponent::BeginPlay()
{
    Super::BeginPlay();
}


void UTopDownInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                               FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UTopDownInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo,
                                                     bool bIsForward /*= true*/)
{
    bool bIsSuccess = false;
    const int32 StepIteration = bIsForward ? 1 : -1;
    int32 IterationAllWeapons = 0;
    int32 IterationSearchWeapon = ChangeToIndex;

    FName NewIdWeapon;
    FAdditionalWeaponInfo NewAdditionalInfo;
    int32 NewCurrentIndex = 0;

    do
    {
        if (IterationSearchWeapon > WeaponSlots.Num() - 1)
        {
            IterationSearchWeapon = 0;
        }
        else if (IterationSearchWeapon < 0)
        {
            IterationSearchWeapon = WeaponSlots.Num() - 1;
        }

        if (WeaponSlots.IsValidIndex(IterationSearchWeapon))
        {
            if (!WeaponSlots[IterationSearchWeapon].NameItem.IsNone())
            {
                if (WeaponSlots[IterationSearchWeapon].AdditionalInfo.Round > 0)
                {
                    //good weapon have ammo start change
                    bIsSuccess = true;
                }
                else
                {
                    UTopDownGameInstance* myGI = Cast<UTopDownGameInstance>(GetWorld()->GetGameInstance());
                    if (myGI)
                    {
                        //check ammoSlots for this weapon
                        FWeaponInfo myInfo;
                        myGI->GetWeaponInfoByName(WeaponSlots[IterationSearchWeapon].NameItem, myInfo);

                        bool bIsFind = false;
                        int8 j = 0;
                        while (j < AmmoSlots.Num() && !bIsFind)
                        {
                            if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
                            {
                                //good weapon have ammo start change
                                bIsSuccess = true;
                                bIsFind = true;
                            }
                            j++;
                        }
                    }
                }
                if (bIsSuccess)
                {
                    NewCurrentIndex = IterationSearchWeapon;
                    NewIdWeapon = WeaponSlots[IterationSearchWeapon].NameItem;
                    NewAdditionalInfo = WeaponSlots[IterationSearchWeapon].AdditionalInfo;
                }
            }
        }

        IterationSearchWeapon += StepIteration;
        IterationAllWeapons++;
    } while (IterationAllWeapons < WeaponSlots.Num() - 1 && !bIsSuccess);

    if (bIsSuccess)
    {
        SetAdditionalInfoWeapon(OldIndex, OldInfo);
        Server_SwitchWeaponEvent(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
    }
    return bIsSuccess;
}

bool UTopDownInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviousIndex,
    FAdditionalWeaponInfo PreviousWeaponInfo)
{
    bool bIsSuccess = false;
    FName ToSwitchIdWeapon;
    FAdditionalWeaponInfo ToSwitchAdditionInfo;

    ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
    ToSwitchAdditionInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

    if (!ToSwitchIdWeapon.IsNone())
    {
        SetAdditionalInfoWeapon(PreviousIndex, PreviousWeaponInfo);
        Server_SwitchWeaponEvent(ToSwitchIdWeapon, ToSwitchAdditionInfo, IndexWeaponToChange);

        EWeaponType ToSwitchWeaponType;
        if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
        {
            int8 AvailableAmmoForWeapon = -1;
            if(CheckAmmoForWeapon(ToSwitchWeaponType, AvailableAmmoForWeapon))
            {
                
            }
        }
        bIsSuccess = true;
    }
    return bIsSuccess;
}

FAdditionalWeaponInfo UTopDownInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
    FAdditionalWeaponInfo result;
    if (WeaponSlots.IsValidIndex(IndexWeapon))
    {
        bool bIsFind = false;
        int8 i = 0;
        while (i < WeaponSlots.Num() && !bIsFind)
        {
            if (i == IndexWeapon)
            {
                result = WeaponSlots[i].AdditionalInfo;
                bIsFind = true;
            }
            i++;
        }
        if (!bIsFind)
        UE_LOG(LogTemp, Warning,
               TEXT("UTopDownInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"),
               IndexWeapon);
    }
    else
    UE_LOG(LogTemp, Warning,
           TEXT("UTopDownInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

    return result;
}

int32 UTopDownInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
    int32 result = -1;
    int8 i = 0;
    bool bIsFind = false;
    while (i < WeaponSlots.Num() && !bIsFind)
    {
        if (WeaponSlots[i].NameItem == IdWeaponName)
        {
            bIsFind = true;
            result = i;
        }
        i++;
    }
    return result;
}

FName UTopDownInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
    FName result;

    if (WeaponSlots.IsValidIndex(IndexSlot))
    {
        result = WeaponSlots[IndexSlot].NameItem;
    }
    return result;
}

bool UTopDownInventoryComponent::GetWeaponTypeBySlotIndex(int32 IndexSlot, EWeaponType& WeaponType)
{
    bool bIsFind = false;
    WeaponType = EWeaponType::Pistol; 
    UTopDownGameInstance* myGI = Cast<UTopDownGameInstance>(GetWorld()->GetGameInstance());
    if (myGI)
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            FWeaponInfo OutInfo;
            myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
            WeaponType = OutInfo.WeaponType;
            bIsFind = true;
        }
    }
    return bIsFind;
}

bool UTopDownInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
    bool bIsFind = false;
    if( !IdWeaponName.IsNone())
    {
        WeaponType = EWeaponType::Pistol; 
        UTopDownGameInstance* myGI = Cast<UTopDownGameInstance>(GetWorld()->GetGameInstance());
        if (myGI)
        {
            FWeaponInfo OutInfo;
            myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
            WeaponType = OutInfo.WeaponType;
            bIsFind = true;
        }
    }
    return bIsFind;
}


void UTopDownInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
    if (WeaponSlots.IsValidIndex(IndexWeapon))
    {
        bool bIsFind = false;
        int8 i = 0;
        while (i < WeaponSlots.Num() && !bIsFind)
        {
            if (i == IndexWeapon)
            {
                WeaponSlots[i].AdditionalInfo = NewInfo;
                bIsFind = true;

                Multicast_WeaponAdditionalInfoChangeEvent(IndexWeapon, NewInfo);
            }
            i++;
        }
        if (!bIsFind)
        UE_LOG(LogTemp, Warning,
               TEXT("UTopDownInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"),
               IndexWeapon);
    }
    else
    UE_LOG(LogTemp, Warning,
           TEXT("UTopDownInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}

void UTopDownInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CountChangeAmmo)
{
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == TypeWeapon)
        {
            AmmoSlots[i].Count += CountChangeAmmo;
            
            if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
            {
                AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
            }
            
            Multicast_AmmoChangeEvent(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);

            bIsFind = true;
        }
        i++;
    }
}

bool UTopDownInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon)
{
    AvailableAmmoForWeapon = 0;
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == TypeWeapon)
        {
            bIsFind = true;
            AvailableAmmoForWeapon = AmmoSlots[i].Count;
            if (AmmoSlots[i].Count > 0)
            {
                return true;
            }
        }

        i++;
    }

    if (AvailableAmmoForWeapon <= 0)
    {
        Multicast_WeaponAmmoEmptyEvent(TypeWeapon);
    }
    else
    {
        Multicast_WeaponAmmoAvailableEvent(TypeWeapon);
    }

    return false;
}

bool UTopDownInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
    bool bResult = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bResult)
    {
        if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
        {
            bResult = true;
        }
        i++;
    }
    return bResult;
}

bool UTopDownInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
    bool bIsFreeSlot = false;
    int8 i = 0;
    while (i < WeaponSlots.Num() && !bIsFreeSlot)
    {
        if (WeaponSlots[i].NameItem.IsNone())
        {
            bIsFreeSlot = true;
            FreeSlot = i;
        }
        i++;
    }
    return bIsFreeSlot;
}

bool UTopDownInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot,
                                                         int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
    bool bResult = false;

    if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
    {
        WeaponSlots[IndexSlot] = NewWeapon;
        SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);
        Multicast_UpdateWeaponSlotsEvent(IndexSlot, NewWeapon);
        bResult = true;
    }
    return bResult;
}

void UTopDownInventoryComponent::Server_TryGetWeaponToInventory_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon)
{
    int32 IndexSlot = -1;
    if (CheckCanTakeWeapon(IndexSlot))
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            WeaponSlots[IndexSlot] = NewWeapon;

            Multicast_UpdateWeaponSlotsEvent(IndexSlot, NewWeapon);

            if (PickUpActor)
            {
                PickUpActor->Destroy();
            }
        }
    }
}

void UTopDownInventoryComponent::Server_DropWeaponByIndex_Implementation(int32 IndexSlot)
{
    FDropItem DropItemInfo;
    
    bool bIsCanDrop = false;
    int32 AvailableWeaponNum = 0;

    for (FWeaponSlot& WeaponSlot: WeaponSlots)
    {
        if (!WeaponSlot.NameItem.IsNone())
        {
            ++AvailableWeaponNum;
        }
    }

    if (AvailableWeaponNum > 1)
    {
        bIsCanDrop = true;
    }

    if (bIsCanDrop && WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
    {

        // switch to next available weapon slot
        for (int i = 0; i < WeaponSlots.Num(); ++i)
        {
            if (!WeaponSlots[i].NameItem.IsNone() && IndexSlot != i)
            {
                Server_SwitchWeaponEvent(WeaponSlots[i].NameItem,  WeaponSlots[i].AdditionalInfo, i);
                break;
            }
        }

        if (GetOwner()->GetClass()->ImplementsInterface(UTopDownGameActor::StaticClass()))
        {
           ITopDownGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo); 
        }

        // clear drop slot
        const FWeaponSlot EmptyWeaponSlot;
        WeaponSlots[IndexSlot] = EmptyWeaponSlot;
        Multicast_UpdateWeaponSlotsEvent(IndexSlot, EmptyWeaponSlot);
    }
}

bool UTopDownInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
    bool result = false;

    FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

    UTopDownGameInstance* myGI = Cast<UTopDownGameInstance>(GetWorld()->GetGameInstance());
    if (myGI)
    {
        result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
        }
    }

    return result;
}

TArray<FWeaponSlot> UTopDownInventoryComponent::GetWeaponSlots()
{
    return WeaponSlots;
}

TArray<FAmmoSlot> UTopDownInventoryComponent::GetAmmoSlots()
{
    return AmmoSlots;
}

void UTopDownInventoryComponent::Server_InitInventory_Implementation(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots)
{
    WeaponSlots = NewWeaponSlots;
    AmmoSlots = NewAmmoSlots;

    MaxSlotsWeapon = WeaponSlots.Num();

    if (WeaponSlots.IsValidIndex(0))
    {
        if (!WeaponSlots[0].NameItem.IsNone())
        {
            Server_SwitchWeaponEvent(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
        }
    }
}

void UTopDownInventoryComponent::Multicast_AmmoChangeEvent_Implementation(EWeaponType TypeWeapon, int32 Count)
{
    OnAmmoChange.Broadcast(TypeWeapon, Count);
}

void UTopDownInventoryComponent::Server_SwitchWeaponEvent_Implementation(FName WeaponIdName,
    FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
    OnSwitchWeapon.Broadcast(WeaponIdName, WeaponAdditionalInfo, NewCurrentIndexWeapon);
}

void UTopDownInventoryComponent::Multicast_WeaponAdditionalInfoChangeEvent_Implementation(int32 IndexSlot,
                                                                                          FAdditionalWeaponInfo AdditionalInfo)
{
    OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UTopDownInventoryComponent::Multicast_WeaponAmmoEmptyEvent_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoEmpty.Broadcast(WeaponType);
}

void UTopDownInventoryComponent::Multicast_WeaponAmmoAvailableEvent_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoAvailable.Broadcast(WeaponType);
}

void UTopDownInventoryComponent::Multicast_UpdateWeaponSlotsEvent_Implementation(int32 IndexSlotChange,
                                                                                 FWeaponSlot NewInfo)
{
    OnUpdateWeaponSlots.Broadcast(IndexSlotChange, NewInfo);
}

void UTopDownInventoryComponent::Multicast_WeaponNotHaveRoundEvent_Implementation(int32 IndexSlotWeapon)
{
}

void UTopDownInventoryComponent::Multicast_WeaponHaveRoundEvent_Implementation(int32 IndexSlotWeapon)
{
}

void UTopDownInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTopDownInventoryComponent, WeaponSlots);
    DOREPLIFETIME(UTopDownInventoryComponent, AmmoSlots);
}

#pragma optimize ("", on)
