// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownCharacterHealthComponent.h"

#include "Net/UnrealNetwork.h"

UTopDownCharacterHealthComponent::UTopDownCharacterHealthComponent()
{
    SetIsReplicatedByDefault(true);
}

void UTopDownCharacterHealthComponent::Server_ChangeHealthValue(float ChangeValue)
{
    if (bIsAlive)
    {
        const float CurrentDamage = ChangeValue * CoefDamage;
        if (Shield > 0.0f && ChangeValue < 0.0f)
        {
            ChangeShieldValue(CurrentDamage);
        }
        else
        {
            Health = FMath::Clamp(Health + CurrentDamage, 0.0f, 100.0f);
            Multicast_HealthChangeEvent(Health, ChangeValue);
            if (Health <= 0.0f)
            {
                bIsAlive = false;
                Multicast_DeadEvent();
            }
        }
    }
}

float UTopDownCharacterHealthComponent::GetCurrentShield()
{
    return Shield;
}

void UTopDownCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
    Shield = FMath::Clamp(Shield + ChangeValue, 0.0f, 100.0f);
    Multicast_ShieldChangeEvent(Shield, ChangeValue);
    if (FMath::IsNearlyZero(Shield))
    {
        //FX 
    }

    GetWorld()->GetTimerManager().SetTimer(CooldownShieldTimerHandle, this,
                                           &UTopDownCharacterHealthComponent::CooldownShieldEnd,
                                           CooldownShieldRecoverTime, false);
    GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimerHandle);
}

void UTopDownCharacterHealthComponent::CooldownShieldEnd()
{
    GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryTimerHandle, this,
                                           &UTopDownCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
}

void UTopDownCharacterHealthComponent::RecoveryShield()
{
    Shield = FMath::Min(Shield + ShieldRecoverValue, 100.0f);
    Multicast_ShieldChangeEvent(Shield, ShieldRecoverValue);
    if (FMath::IsNearlyEqual(Shield, 100.0f))
    {
        GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimerHandle);
    }
}

void UTopDownCharacterHealthComponent::Multicast_ShieldChangeEvent_Implementation(float CurrentShield, float Damage)
{
    OnShieldChange.Broadcast(CurrentShield, Damage);
}

void UTopDownCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTopDownCharacterHealthComponent, Shield);
}
