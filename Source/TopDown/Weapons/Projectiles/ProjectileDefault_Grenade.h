#pragma once

#include "CoreMinimal.h"
#include "TopDown/Weapons/Projectiles/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

UCLASS()
class TOPDOWN_API AProjectileDefault_Grenade : public AProjectileDefault
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Grenade")
    bool TimerEnabled = false;

    float TimerToExplosion = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Grenade")
    float TimeToExplosion = 5.0f;

    virtual void Tick(float DeltaTime) override;

    virtual void InitProjectile(FProjectileInfo InitParam) override;

    void TimerExplosion(float DeltaTime);


    virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                          UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                          const FHitResult& Hit) override;
    virtual void ImpactProjectile() override;

    void Explosion();
};
