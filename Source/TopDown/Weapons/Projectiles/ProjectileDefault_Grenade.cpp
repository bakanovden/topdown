#include "ProjectileDefault_Grenade.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
    TEXT("TPS.DebugExplode"),
    DebugExplodeShow,
    TEXT("Draw Debug for Explode"),
    ECVF_Cheat
);

void AProjectileDefault_Grenade::BeginPlay()
{
    Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    TimerExplosion(DeltaTime);
}

void AProjectileDefault_Grenade::InitProjectile(FProjectileInfo InitParam)
{
    Super::InitProjectile(InitParam);
    TimeToExplosion = ProjectileSetting.ProjectileLifeTime - 0.5;
}

void AProjectileDefault_Grenade::TimerExplosion(float DeltaTime)
{
    if (TimerEnabled)
    {
        if (TimerToExplosion > TimeToExplosion)
        {
            Explosion();
        }
        else
        {
            TimerToExplosion += DeltaTime;
        }
    }
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                                          UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                                          const FHitResult& Hit)
{
    if (!TimerEnabled)
    {
        Explosion(); 
    } 
    Super::BulletCollisionSphereHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
    TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explosion()
{
    if (DebugExplodeShow)
    {
        DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage,
                        12, FColor::Green, false, 12.0f);
        DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage,
                        12, FColor::Red, false, 12.0f);
    }

    TimerEnabled = false;

    if (ProjectileSetting.ExplosionFX)
    {
        // UGameplayStatics::SpawnEmitterAtLocation(
        //     GetWorld(), ProjectileSetting.ExplosionFX, GetActorLocation(),
        //     GetActorRotation(), FVector::OneVector
        // );
        Multicast_SpawnHitVFX(ProjectileSetting.ExplosionFX, GetActorLocation());
    }

    if (ProjectileSetting.ExplosionSound)
    {
        Multicast_SpawnHitSFX(ProjectileSetting.ExplosionSound, GetActorLocation());
    }

    TArray<AActor*> IgnoredActor;
    UGameplayStatics::ApplyRadialDamageWithFalloff(
        GetWorld(),
        ProjectileSetting.ExplosionMaxDamage,
        ProjectileSetting.ExplosionMaxDamage * 0.2f,
        GetActorLocation(),
        ProjectileSetting.ProjectileMinRadiusDamage,
        ProjectileSetting.ProjectileMaxRadiusDamage,
        5,
        NULL,
        IgnoredActor,
        this,
        nullptr
    );

    this->Destroy();
}
