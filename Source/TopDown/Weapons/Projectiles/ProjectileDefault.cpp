// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"

#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"
#include "TopDown/Interfaces/TopDownGameActor.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    bReplicates = true;

    BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("BulletCollisionSphere"));
    BulletCollisionSphere->SetSphereRadius(16.6f);
    BulletCollisionSphere->bReturnMaterialOnMove = true;
    BulletCollisionSphere->SetCanEverAffectNavigation(false);

    RootComponent = BulletCollisionSphere;

    BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletMesh"));
    BulletMesh->SetupAttachment(RootComponent);
    BulletMesh->SetCanEverAffectNavigation(false);

    BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletFX"));
    BulletFX->SetupAttachment(RootComponent);

    BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BulletProjectileMovement"));
    BulletProjectileMovement->UpdatedComponent = RootComponent;

    BulletProjectileMovement->bRotationFollowsVelocity = true;
    BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
    Super::BeginPlay();

    BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
    BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(
        this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
    BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}


void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
    BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
    BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;
    this->SetLifeSpan(InitParam.ProjectileLifeTime);

    if (InitParam.ProjectileStaticMesh)
    {
        Multicast_InitVisualMeshProjectile(InitParam.ProjectileStaticMesh, InitParam.ProjectileStaticMeshOffset);
    }
    else
    {
        BulletMesh->DestroyComponent();
    }

    if (InitParam.ProjectileTrailFx)
    {
        Multicast_InitVisualTrailProjectile(InitParam.ProjectileTrailFx, InitParam.ProjectileTrailFxOffset);
    }
    else
    {
        BulletFX->DestroyComponent();
    }

	Multicast_InitVelocity(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);

    ProjectileSetting = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                                  UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                                  const FHitResult& Hit)
{
    if (OtherActor && Hit.PhysMaterial.IsValid())
    {
        EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

        if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
        {
            UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];

            if (myMaterial && OtherComp)
            {
                Multicast_SpawnHitDecal(myMaterial, OtherComp, Hit);
            }
        }

        if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
        {
            UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];

            if (myParticle)
            {
                Multicast_SpawnHitVFX(myParticle, Hit.ImpactPoint);
            }
        }

        if (ProjectileSetting.HitSound)
        {
            Multicast_SpawnHitSFX(ProjectileSetting.HitSound, Hit.ImpactPoint);
        }

        UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileSetting.Effect, mySurfacetype);
    }

    UGameplayStatics::ApplyPointDamage(
        OtherActor,
        ProjectileSetting.ProjectileDamage,
        Hit.TraceStart,
        Hit,
        GetInstigatorController(),
        this,
        NULL
    );

    UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage,
                                       Hit.Location, Hit.Location);
    ImpactProjectile();
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                           UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
                                                           bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                         UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::PostNetReceiveVelocity(const FVector& NewVelocity)
{
    Super::PostNetReceiveVelocity(NewVelocity);

    if (BulletProjectileMovement)
    {
        BulletProjectileMovement->Velocity = NewVelocity;
    }
}

void AProjectileDefault::ImpactProjectile()
{
    this->Destroy();
}

void AProjectileDefault::Multicast_InitVisualMeshProjectile_Implementation(UStaticMesh* NewMesh,
                                                                           FTransform MeshRelative)
{
    BulletMesh->SetStaticMesh(NewMesh);
    BulletMesh->SetRelativeTransform(MeshRelative);
}

void AProjectileDefault::Multicast_InitVisualTrailProjectile_Implementation(UParticleSystem* NewTrail,
                                                                            FTransform TrailRelative)
{
    BulletFX->SetTemplate(NewTrail);
    BulletFX->SetRelativeTransform(TrailRelative);
}

void AProjectileDefault::Multicast_SpawnHitDecal_Implementation(UMaterialInterface* DecalMaterial,
                                                                UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
    UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint,
                                         HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition,
                                         10.0f);
}

void AProjectileDefault::Multicast_SpawnHitVFX_Implementation(UParticleSystem* FxTemplate, FVector HitLocation)
{
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate,
                                             FTransform(HitLocation.Rotation(), HitLocation, FVector::OneVector));
}

void AProjectileDefault::Multicast_SpawnHitSFX_Implementation(USoundBase* HitSound, FVector HitLocation)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitLocation);
}

void AProjectileDefault::Multicast_InitVelocity_Implementation(float InitSpeed, float MaxSpeed)
{
    if (BulletProjectileMovement)
    {
        BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
        BulletProjectileMovement->MaxSpeed = MaxSpeed;
        BulletProjectileMovement->InitialSpeed = InitSpeed;
    }
}
