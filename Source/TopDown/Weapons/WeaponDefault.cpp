// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TopDown/TopDownStateEffect.h"
#include "TopDown/Character/TopDownInventoryComponent.h"
#include "TopDown/Interfaces/TopDownGameActor.h"
#include "Net/UnrealNetwork.h"
#include "TopDown/TopDown.h"

int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(
    TEXT("TPS.DebugWeapon"),
    DebugWeaponShow,
    TEXT("Draw Debug for Weapon"),
    ECVF_Cheat
);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
    //Network
    bReplicates = true;

    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
    RootComponent = SceneComponent;

    SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
    SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    SkeletalMeshWeapon->SetupAttachment(RootComponent);

    ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
    ShootLocation->SetupAttachment(RootComponent);

    SpawnCasingLocation = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnCasingLocation"));
    SpawnCasingLocation->SetupAttachment(SkeletalMeshWeapon);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
    Super::BeginPlay();

    WeaponInit();
}

void AWeaponDefault::WeaponInit()
{
    if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
        SkeletalMeshWeapon->DestroyComponent(true);

    if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
        StaticMeshWeapon->DestroyComponent(true);
    Server_UpdateStateWeapon(EMovementState::Run_State);
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (HasAuthority())
    {
        FireTick(DeltaTime);
        ReloadTick(DeltaTime);
        DispersionTick(DeltaTime);
        ClipDropTick(DeltaTime);
        ShellDropTick(DeltaTime);
    }
}

void AWeaponDefault::FireTick(float DeltaTime)
{
    if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
    {
        if (FireTimer < 0.0f)
            Fire();
        else
            FireTimer -= DeltaTime;
    }
}

void AWeaponDefault::Fire()
{
    // On Server by Weaponfire bool
    if (WeaponSetting.ShellBullets.DropMesh)
    {
        if (WeaponSetting.ShellBullets.DropMeshTime <= 0.0f)
        {
            Server_InitDropMesh(WeaponSetting.ShellBullets);
        }
        else
        {
            bDropShellFlag = true;
            DropShellTimer = WeaponSetting.ShellBullets.DropMeshLifeTime;
        }
    }


    UAnimMontage* AnimToPlay = nullptr;
    if (bWeaponAiming)
    {
        AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharFireAim;
    }
    else
    {
        AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharFire;
    }

    FireTimer = WeaponSetting.RateOfFire;
    AdditionalWeaponInfo.Round--;
    ChangeDispersionByShot();

    OnWeaponFireStart.Broadcast(AnimToPlay);

    Multicast_WeaponFireFX(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

    if (ShootLocation)
    {
        FVector SpawnLocation = ShootLocation->GetComponentLocation();
        FRotator SpawnRotation = ShootLocation->GetComponentRotation();
        FVector SpawnScale(FVector::OneVector);
        FProjectileInfo ProjectileInfo = GetProjectile();

        FVector EndLocation;
        int8 NumberProjectileByShot = GetNumberProjectileByShot();
        for (int8 i = 0; i < NumberProjectileByShot; i++)
        {
            EndLocation = GetFireEndLocation();
            FProjectileInfo ProjectileSetting = WeaponSetting.ProjectileSetting;
            if (ProjectileInfo.Projectile)
            {
                FVector Dir = EndLocation - SpawnLocation;

                Dir.Normalize();

                FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
                SpawnRotation = myMatrix.Rotator();
                FTransform SpawnTransform(SpawnRotation, SpawnLocation, SpawnScale);

                FActorSpawnParameters SpawnParameters;
                SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                SpawnParameters.Owner = GetOwner();
                SpawnParameters.Instigator = GetInstigator();

                AProjectileDefault* myProjectile = Cast<AProjectileDefault>(
                    GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnTransform, SpawnParameters));
                if (myProjectile)
                {
                    myProjectile->InitProjectile(ProjectileSetting);
                }
            }
            else
            {
                FHitResult Hit;
                TArray<AActor*> Actors;
                Actors.Add(GetOwner());

                UKismetSystemLibrary::LineTraceSingle(
                    GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
                    TRACE_CHANNEL_PROJECTILE, false, Actors, EDrawDebugTrace::ForDuration, Hit,
                    true, FLinearColor::Red, FLinearColor::Green, 5.0f);

                if (DebugWeaponShow == 1)
                {
                    FVector LineEnd = SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace;
                    DrawDebugLine(GetWorld(), SpawnLocation, LineEnd, FColor::Black,
                    false, 5.0f, '\000', 0.5f);
                    if (Hit.bBlockingHit)
                    {
                        DrawDebugPoint(GetWorld(), Hit.Location, 10.0f, FColor::Blue, false, 5.0f);
                    }
                }

                if (Hit.bBlockingHit && Hit.PhysMaterial.IsValid())
                {
                    EPhysicalSurface HitSurfaceType = UGameplayStatics::GetSurfaceType(Hit);
                    if (ProjectileSetting.HitDecals.Contains(HitSurfaceType))
                    {
                        UMaterialInterface* HitReactionDecalMaterial = ProjectileSetting.HitDecals[
                            HitSurfaceType];
                        if (HitReactionDecalMaterial && Hit.GetComponent())
                        {
                            Multicast_SpawnHitDecal(HitReactionDecalMaterial, Hit.GetComponent(), Hit);
                        }
                    }

                    if (ProjectileSetting.HitFXs.Contains(HitSurfaceType))
                    {
                        UParticleSystem* myParticle = ProjectileSetting.HitFXs[HitSurfaceType];

                        if (myParticle)
                        {
                            Multicast_SpawnHitVFX(myParticle, Hit);
                        }
                    }

                    if (ProjectileSetting.HitSound)
                    {
                        Multicast_SpawnHitSFX(ProjectileSetting.HitSound, Hit);
                    }

                    UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, HitSurfaceType);

                    UGameplayStatics::ApplyDamage(Hit.GetActor(), ProjectileSetting.ProjectileDamage,
                                                  GetInstigatorController(), this,
                                                  NULL);
                }
            }
        }
    }

    if (GetWeaponRound() <= 0 && !bWeaponReloading)
    {
        if (CheckCanWeaponReload())
        {
            InitReload();
        }
    }
}

void AWeaponDefault::Server_UpdateStateWeapon_Implementation(EMovementState MovementState)
{
    bBlockFire = false;

    switch (MovementState)
    {
    case EMovementState::Aim_State:
        bWeaponAiming = true;
        CurrentDispersionMax = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Aim_StateDispersionReduction;
        break;
    case EMovementState::Aim_Walk_State:
        bWeaponAiming = true;
        CurrentDispersionMax = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionReduction;
        break;
    case EMovementState::Walk_State:
        bWeaponAiming = false;
        CurrentDispersionMax = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Walk_StateDispersionReduction;
        break;
    case EMovementState::Run_State:
        bWeaponAiming = false;
        CurrentDispersionMax = WeaponSetting.WeaponDispersion.Run_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.WeaponDispersion.Run_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Run_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Run_StateDispersionReduction;
        break;
    case EMovementState::Sprint_Run_State:
        bWeaponAiming = false;
        bBlockFire = true;
        Server_SetWeaponStateFire(false);
        break;
    default:
        break;
    }
}

void AWeaponDefault::ChangeDispersion()
{
}

void AWeaponDefault::Server_SetWeaponStateFire_Implementation(bool bIsFire)
{
    if (CheckWeaponCanFire())
    {
        bWeaponFiring = bIsFire;
    }
    else
    {
        bWeaponFiring = false;
        FireTimer = 0.01f;
    }
}

bool AWeaponDefault::CheckWeaponCanFire()
{
    return !bBlockFire;
}


FProjectileInfo AWeaponDefault::GetProjectile()
{
    return WeaponSetting.ProjectileSetting;
}


void AWeaponDefault::ReloadTick(float DeltaTime)
{
    if (bWeaponReloading)
    {
        if (ReloadTimer < 0.0f)
        {
            FinishReload();
        }
        else
        {
            ReloadTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
    if (!bWeaponReloading)
    {
        if (!bWeaponFiring)
        {
            if (bShouldReduceDispersion)
                CurrentDispersion -= CurrentDispersionReduction;
            else
                CurrentDispersion += CurrentDispersionReduction;
        }

        if (CurrentDispersion < CurrentDispersionMin)
            CurrentDispersion = CurrentDispersionMin;
        else if (CurrentDispersion > CurrentDispersionMax)
            CurrentDispersion = CurrentDispersionMax;
    }

    if (DebugWeaponShow == 1)
    {
        UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax,
               CurrentDispersionMin, CurrentDispersion);
    }
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
    if (bDropClipFlag)
    {
        if (DropClipTimer < 0.0f)
        {
            bDropClipFlag = false;
            Server_InitDropMesh(WeaponSetting.ClipDropMesh);
        }
        else
        {
            DropClipTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
    if (bDropShellFlag)
    {
        if (DropShellTimer < 0.0f)
        {
            bDropShellFlag = false;
            Server_InitDropMesh(WeaponSetting.ShellBullets);
        }
        else
        {
            DropShellTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::ChangeDispersionByShot()
{
    CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
    float Result = CurrentDispersion;
    return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
    return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
    bool bShootDirection = false;
    FVector EndLocation;
    FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

    float AngleWidth = GetCurrentDispersion() * PI / 180.0f;
    if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
    {
        EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(tmpV.GetSafeNormal()) * -20000.0f;
        if (DebugWeaponShow == 1)
        {
            DrawDebugCone(
                GetWorld(), ShootLocation->GetComponentLocation(),
                -(ShootLocation->GetComponentLocation() - ShootEndLocation),
                WeaponSetting.DistanceTrace, AngleWidth, AngleWidth,
                32, FColor::Yellow, false,
                5.0f, (uint8)'\000', 1.0f
            );
        }
    }
    else
    {
        EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector())
            * 20000.0f;
        if (DebugWeaponShow == 1)
            DrawDebugCone(
                GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(),
                WeaponSetting.DistanceTrace, AngleWidth, AngleWidth,
                32, FColor::Orange, false, 5.0f,
                (uint8)'\000', 1.0f
            );
    }

    if (DebugWeaponShow == 1)
    {
        //direction weapon look
        DrawDebugLine(
            GetWorld(), ShootLocation->GetComponentLocation(),
            ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f,
            FColor::Cyan, false, 5.0f, (uint8)'\000', 0.5f
        );
        //Direction Projectile Current fly
        DrawDebugLine(
            GetWorld(), ShootLocation->GetComponentLocation(),
            EndLocation, FColor::White, false, 5.0f,
            (uint8)'\000', 0.5f
        );

        //DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
    }

    return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
    return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
    return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
    bWeaponReloading = true;

    ReloadTimer = WeaponSetting.ReloadTime;

    UAnimMontage* AnimToPlay = nullptr;
    if (bWeaponAiming)
    {
        AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharReloadAim;
    }
    else
    {
        AnimToPlay = WeaponSetting.AnimationWeaponInfo.AnimCharReload;
    }

    OnWeaponReloadStart.Broadcast(AnimToPlay);

    UAnimMontage* AnimWeaponToPlay = nullptr;
    if (bWeaponAiming)
    {
        AnimWeaponToPlay = WeaponSetting.AnimationWeaponInfo.AnimWeaponReloadAim;
    }
    else
    {
        AnimWeaponToPlay = WeaponSetting.AnimationWeaponInfo.AnimWeaponReload;
    }

    if (AnimWeaponToPlay)
    {
        Multicast_AnimWeapon(AnimWeaponToPlay);
    }

    if (WeaponSetting.ClipDropMesh.DropMesh)
    {
        bDropClipFlag = true;
        DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
    }
}

void AWeaponDefault::FinishReload()
{
    bWeaponReloading = false;

    int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
    int8 AmmoNeedTakeFromInv;
    int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;
    NeedToReload = FMath::Min(NeedToReload, AvailableAmmoFromInventory);

    AdditionalWeaponInfo.Round += NeedToReload;
    AmmoNeedTakeFromInv = NeedToReload;

    OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
    bWeaponReloading = false;
    if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
    {
        SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
    }

    OnWeaponReloadEnd.Broadcast(false, 0);
    bDropClipFlag = false;
}

void AWeaponDefault::Server_InitDropMesh_Implementation(const FDropMeshInfo& ShellBullets)
{
    if (ShellBullets.DropMesh)
    {
        Multicast_ShellDropFireMesh(ShellBullets);
    }
}

bool AWeaponDefault::CheckCanWeaponReload()
{
    bool result = true;
    if (GetOwner())
    {
        UTopDownInventoryComponent* myInv = Cast<UTopDownInventoryComponent>(
            GetOwner()->GetComponentByClass(UTopDownInventoryComponent::StaticClass()));
        if (myInv)
        {
            int8 AvailableAmmoForWeapon;
            if (!myInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
            {
                result = false;
                myInv->OnWeaponNotHaveRound.Broadcast(myInv->GetWeaponIndexSlotByName(IdWeaponName));
            }
            else
            {
                myInv->OnWeaponHaveRound.Broadcast(myInv->GetWeaponIndexSlotByName(IdWeaponName));
            }
        }
    }

    return result;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
    int8 AvailableAmmoForWeapon = WeaponSetting.MaxRound;
    if (GetOwner())
    {
        UTopDownInventoryComponent* MyInv = Cast<UTopDownInventoryComponent>(
            GetOwner()->GetComponentByClass(UTopDownInventoryComponent::StaticClass()));
        if (MyInv)
        {
            MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon);
        }
    }
    return AvailableAmmoForWeapon;
}

void AWeaponDefault::Server_UpdateWeaponByCharacterMovementState_Implementation(FVector NewShootEndLocation,
    bool NewShouldReduceDispersion)
{
    ShootEndLocation = NewShootEndLocation;
    bShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefault::Multicast_AnimWeapon_Implementation(UAnimMontage* Anim)
{
    if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
    {
        SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim);
    }
}

void AWeaponDefault::Multicast_ShellDropFireMesh_Implementation(const FDropMeshInfo& ShellBullets)
{
    if (ShellBullets.DropMesh)
    {
        FTransform Transform;

        FVector OffsetLocatin = ShellBullets.DropMeshOffset.GetLocation();
        FVector LocalDir = GetActorForwardVector() * OffsetLocatin.X + GetActorRightVector() * OffsetLocatin.Y +
            GetActorUpVector() * OffsetLocatin.Z;

        Transform.SetLocation(GetActorLocation() + LocalDir);
        Transform.SetScale3D(ShellBullets.DropMeshOffset.GetScale3D());
        Transform.SetRotation((GetActorRotation() + ShellBullets.DropMeshOffset.Rotator()).Quaternion());

        FActorSpawnParameters Param;
        Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
        Param.Owner = this;

        AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(
            AStaticMeshActor::StaticClass(), Transform, Param);

        if (NewActor && NewActor->GetStaticMeshComponent())
        {
            NewActor->SetActorTickEnabled(true);
            NewActor->SetLifeSpan(ShellBullets.DropMeshLifeTime);

            UStaticMeshComponent* MeshComponent = NewActor->GetStaticMeshComponent();
            MeshComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
            MeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
            MeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
            MeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
            MeshComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
            MeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
            MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
            MeshComponent->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

            MeshComponent->Mobility = EComponentMobility::Movable;
            MeshComponent->SetSimulatePhysics(true);
            MeshComponent->SetStaticMesh(ShellBullets.DropMesh);

            if (ShellBullets.CustomMass > 0.0f)
            {
                MeshComponent->SetMassOverrideInKg(NAME_None, ShellBullets.CustomMass, true);
            }

            if (!ShellBullets.DropMeshImpulseDirOffset.IsNearlyZero())
            {
                FVector FinalDir;
                LocalDir = LocalDir + (ShellBullets.DropMeshImpulseDirOffset * 1000.0f);

                if (!FMath::IsNearlyZero(ShellBullets.ImpulseRandomDispersion))
                {
                    FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(
                        LocalDir, ShellBullets.ImpulseRandomDispersion);
                }
                FinalDir.Normalize();

                MeshComponent->AddImpulse(FinalDir * ShellBullets.PowerImpulse);
            }
        }
    }
}

void AWeaponDefault::Multicast_WeaponFireFX_Implementation(UParticleSystem* FxFire, USoundBase* SoundFire)
{
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
}

void AWeaponDefault::Multicast_SpawnHitDecal_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
    UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint,
                                         HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition,
                                         10.0f);
}

void AWeaponDefault::Multicast_SpawnHitVFX_Implementation(UParticleSystem* FxTemplate, FHitResult HitResult)
{
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate,
                                             FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint,
                                                        FVector(1.0f)));
}

void AWeaponDefault::Multicast_SpawnHitSFX_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);

    DOREPLIFETIME(AWeaponDefault, bWeaponReloading);
    DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
}
