// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TopDown/FunctionLibrary/Types.h"
#include "TopDown/Weapons/Projectiles/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

class ITopDownGameActor;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TOPDOWN_API AWeaponDefault : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AWeaponDefault();

    FOnWeaponFireStart OnWeaponFireStart;
    FOnWeaponReloadEnd OnWeaponReloadEnd;
    FOnWeaponReloadStart OnWeaponReloadStart;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USceneComponent* SceneComponent = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UStaticMeshComponent* StaticMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UArrowComponent* ShootLocation = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USceneComponent* SpawnCasingLocation = nullptr;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = WeaponInfo)
    FAdditionalWeaponInfo AdditionalWeaponInfo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
    FName IdWeaponName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FireLogic)
    FWeaponInfo WeaponSetting;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FireLogic)
    bool bWeaponFiring = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FireLogic)
    float FireTimer = 0.0f;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = ReloadLogic)
    bool bWeaponReloading = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ReloadLogic)
    float ReloadTimer = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
    float ReloadTime = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
    float SizeVectorToChangeShootDirectionLogic = 100.0f;

    bool bBlockFire = false;
    bool bShouldReduceDispersion = false;
    float CurrentDispersion = 0.0f;
    float CurrentDispersionMax = 1.0f;
    float CurrentDispersionMin = 0.1f;
    float CurrentDispersionRecoil = 0.1f;
    float CurrentDispersionReduction = 0.1f;

    bool bDropClipFlag = false;
    float DropClipTimer = -1.0f;
    bool bDropShellFlag = false;
    float DropShellTimer = -1.0f;

    bool bWeaponAiming = false;

    UPROPERTY(Replicated)
    FVector ShootEndLocation = FVector::ZeroVector;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    void FireTick(float DeltaTime);
    void ReloadTick(float DeltaTime);
    void DispersionTick(float DeltaTime);
    void ClipDropTick(float DeltaTime);
    void ShellDropTick(float DeltaTime);

    void WeaponInit();

    UFUNCTION(Server, Reliable, BlueprintCallable)
    void Server_SetWeaponStateFire(bool bIsFire);

    bool CheckWeaponCanFire();

    FProjectileInfo GetProjectile();

    void Fire();
    UFUNCTION(Server, Reliable)
    void Server_UpdateStateWeapon(EMovementState MovementState);
    void ChangeDispersion();

    void ChangeDispersionByShot();
    float GetCurrentDispersion() const;
    FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

    FVector GetFireEndLocation() const;
    int8 GetNumberProjectileByShot() const;

    UFUNCTION(BlueprintCallable)
    int32 GetWeaponRound();
    void InitReload();
    void FinishReload();
	void CancelReload();

    UFUNCTION(Server, Reliable)
    void Server_InitDropMesh(const FDropMeshInfo& ShellBullets);

    bool CheckCanWeaponReload();
    int8 GetAvailableAmmoForReload();

    UFUNCTION(Server, Unreliable)
    void Server_UpdateWeaponByCharacterMovementState(FVector NewShootEndLocation, bool bNewShouldReduceDispersion);

    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_AnimWeapon(UAnimMontage* Anim);

    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_ShellDropFireMesh(const FDropMeshInfo& ShellBullets);

    UFUNCTION(NetMulticast, Unreliable)
    void Multicast_WeaponFireFX(UParticleSystem* FxFire, USoundBase* SoundFire);

    UFUNCTION(NetMulticast, Reliable)
    void Multicast_SpawnHitDecal(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);

    UFUNCTION(NetMulticast, Reliable)
    void Multicast_SpawnHitVFX(UParticleSystem* FxTemplate, FHitResult HitResult);

    UFUNCTION(NetMulticast, Reliable)
    void Multicast_SpawnHitSFX(USoundBase* HitSound, FHitResult HitResult);
};
