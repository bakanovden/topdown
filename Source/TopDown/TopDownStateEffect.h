// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Particles/ParticleEmitter.h"
#include "UObject/NoExportTypes.h"
#include "TopDownStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWN_API UTopDownStateEffect : public UObject
{
    GENERATED_BODY()
public:

    UPROPERTY()
    AActor* Target = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting")
    TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting")
    bool bIsStackable = false;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Particle Setting")
    UParticleSystem* ParticleEffect;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Particle Setting")
    bool bIsAutoDestroyParticleEffect = false;

    UPROPERTY(Replicated)
    FName NameBone = NAME_None;

    virtual bool IsSupportedForNetworking() const override { return true; }; 
    virtual bool InitObject(AActor* NewTarget, FName NameBoneHit);
    virtual void DestroyObject();
};

UCLASS(Blueprintable, BlueprintType)
class TOPDOWN_API UTopDownStateEffect_ExecuteOnce : public UTopDownStateEffect
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting Execute Once")
    float Power = 20.0f;

    virtual bool InitObject(AActor* NewTarget, FName NameBoneHit) override;
    virtual void DestroyObject() override;

    virtual void ExecuteOnce();
};

UCLASS(Blueprintable, BlueprintType)
class TOPDOWN_API UTopDownStateEffect_ExecuteTimer : public UTopDownStateEffect
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting Execute Timer")
    float Power = 20.0f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting Execute Timer")
    float Timer = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect Setting Execute Timer")
    float RateTime = 1.0f;

    UPROPERTY()
    FTimerHandle ExecuteTimerHandle;
    UPROPERTY()
    FTimerHandle EffectTimerHandle;

    virtual bool InitObject(AActor* NewTarget, FName NameBoneHit) override;
    virtual void DestroyObject() override;

    virtual void Execute();
};
