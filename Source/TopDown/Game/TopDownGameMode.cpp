// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownGameMode.h"
#include "TopDownPlayerController.h"
#include "TopDown/Character/TopDownCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopDownGameMode::ATopDownGameMode()
{
    // use our custom PlayerController class
    PlayerControllerClass = ATopDownPlayerController::StaticClass();
}

void ATopDownGameMode::PlayerCharacterDead()
{
}
