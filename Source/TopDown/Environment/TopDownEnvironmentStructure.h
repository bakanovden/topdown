#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TopDown/TopDownStateEffect.h"
#include "TopDown/Interfaces/TopDownGameActor.h"

#include "TopDownEnvironmentStructure.generated.h"

UCLASS()
class TOPDOWN_API ATopDownEnvironmentStructure : public AActor, public ITopDownGameActor
{
	GENERATED_BODY()
	
public:	
	ATopDownEnvironmentStructure();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
	class UTopDownStateEffectComponent* StateEffectComponent;

protected:
	virtual void BeginPlay() override;

public:	
	//Interface ITopDownGameActor
	virtual EPhysicalSurface GetSurfaceType_Implementation() override;

	virtual TArray<UTopDownStateEffect*> GetAllCurrentEffects_Implementation() override;

	virtual void RemoveEffect_Implementation(UTopDownStateEffect* RemoveEffect) override;

	virtual void AddEffect_Implementation(UTopDownStateEffect* NewEffect) override;
	//end Interface ITopDownGameActor
};
