#include "TopDownEnvironmentStructure.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TopDown/Components/TopDownStateEffectComponent.h"

ATopDownEnvironmentStructure::ATopDownEnvironmentStructure()
{
	PrimaryActorTick.bCanEverTick = false;

	StateEffectComponent = CreateDefaultSubobject<UTopDownStateEffectComponent>(TEXT("StateEffects"));
}

void ATopDownEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

EPhysicalSurface ATopDownEnvironmentStructure::GetSurfaceType_Implementation()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if(myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if(myMaterial)
		{
			UPhysicalMaterial* PhysicalMaterial = myMaterial->GetPhysicalMaterial();
			Result = PhysicalMaterial->SurfaceType;
		}
	}
	return Result;
}

TArray<UTopDownStateEffect*> ATopDownEnvironmentStructure::GetAllCurrentEffects_Implementation()
{
	return StateEffectComponent->GetEffects();
}

void ATopDownEnvironmentStructure::RemoveEffect_Implementation(UTopDownStateEffect* RemoveEffect)
{
	StateEffectComponent->RemoveEffect(RemoveEffect);
}

void ATopDownEnvironmentStructure::AddEffect_Implementation(UTopDownStateEffect* NewEffect)
{
	StateEffectComponent->AddEffect(NewEffect);
}

