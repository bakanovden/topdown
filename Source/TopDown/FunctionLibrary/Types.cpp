// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"

#include "TopDown/Interfaces/TopDownGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit,
                                    TSubclassOf<UTopDownStateEffect> AddEffectClass,
                                    EPhysicalSurface SurfaceType)
{
    if (SurfaceType != SurfaceType_Default && AddEffectClass && TakeEffectActor && TakeEffectActor->Implements<UTopDownGameActor>())
    {
        UTopDownStateEffect* myEffect = Cast<UTopDownStateEffect>(AddEffectClass.GetDefaultObject());
        if (myEffect && (myEffect->bIsStackable || !HasEffect(TakeEffectActor, myEffect)))
        {
            bool bIsCanAdd = false;
            int32 i = 0;
            while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
            {
                if (myEffect->PossibleInteractSurface[i] == SurfaceType)
                {
                    bIsCanAdd = true;
                    UTopDownStateEffect* NewEffect = NewObject<UTopDownStateEffect>(TakeEffectActor, AddEffectClass);
                    if (NewEffect)
                    {
                        NewEffect->InitObject(TakeEffectActor, NameBoneHit);
                    }
                }
                ++i;
            }
        }
    }
}

bool UTypes::HasEffect(AActor* TakeEffectActor, UTopDownStateEffect* TargetEffect)
{
    bool Result = false;
    if (TakeEffectActor->Implements<UTopDownGameActor>())
    {
        TArray<UTopDownStateEffect*> TopDownStateEffects = ITopDownGameActor::Execute_GetAllCurrentEffects(
            TakeEffectActor);
        int32 i = 0;
        while (i < TopDownStateEffects.Num() && !Result)
        {
            Result = TopDownStateEffects[i]->GetClass() == TargetEffect->GetClass();
            ++i;
        }
    }
    return Result;
}
