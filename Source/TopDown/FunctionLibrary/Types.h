// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TopDown/TopDownStateEffect.h"

#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    Aim_Walk_State UMETA(DisplayName = "Aim Walk State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    Sprint_Run_State UMETA(DisplayName = "Sprint Run State"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
    Rifle UMETA(DisplayName = "Rifle"),
    Pistol UMETA(DisplayName = "Pistol"),
    Shotgun UMETA(DisplayName = "Shotgun"),
    GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeed = 300.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimWalkSpeed = 100.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeed = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeed = 600.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintRunSpeed = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    TSubclassOf<class AProjectileDefault> Projectile = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    UStaticMesh* ProjectileStaticMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    FTransform ProjectileStaticMeshOffset;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    UParticleSystem* ProjectileTrailFx = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    FTransform ProjectileTrailFxOffset;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileDamage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileInitSpeed = 2000.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileMaxSpeed = 2000.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileLifeTime = 20.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* HitSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
    TSubclassOf<UTopDownStateEffect> Effect;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
    UParticleSystem* ExplosionFX = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
    USoundBase* ExplosionSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
    float ProjectileMinRadiusDamage = 500.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
    float ProjectileMaxRadiusDamage = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
    float ExplosionMaxDamage = 40.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Aim_StateDispersionAimMax = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Aim_StateDispersionAimMin = 0.3f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Aim_StateDispersionAimRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Aim_StateDispersionReduction = 0.3f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimWalk_StateDispersionAimMax = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimWalk_StateDispersionAimMin = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimWalk_StateDispersionAimRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimWalk_StateDispersionReduction = 0.4f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Walk_StateDispersionAimMax = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Walk_StateDispersionAimMin = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Walk_StateDispersionAimRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Walk_StateDispersionReduction = 0.2f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Run_StateDispersionAimMax = 10.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Run_StateDispersionAimMin = 4.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Run_StateDispersionAimRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    UStaticMesh* DropMesh = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    float DropMeshTime = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    float DropMeshLifeTime = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    FTransform DropMeshOffset;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    FVector DropMeshImpulseDirOffset;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    float PowerImpulse = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    float ImpulseRandomDispersion = 10.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Clip Drop Mesh")
    float CustomMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimCharFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimCharFireAim = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimCharReload = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimCharReloadAim = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimWeaponFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimWeaponReload = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    UAnimMontage* AnimWeaponReloadAim = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponSetting)
    TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = State)
    float RateOfFire = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = State)
    float ReloadTime = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = State)
    int32 MaxRound = 10;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = State)
    int32 NumberProjectileByShot = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Dispersion)
    FWeaponDispersion WeaponDispersion;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
    USoundBase* SoundFireWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
    USoundBase* SoundReloadWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FX)
    UParticleSystem* EffectFireWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
    FProjectileInfo ProjectileSetting;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trace)
    float WeaponDamage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trace)
    float DistanceTrace = 2000.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HitEffect)
    UDecalComponent* DecalOnHit = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anim)
    FAnimationWeaponInfo AnimationWeaponInfo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
    FDropMeshInfo ClipDropMesh;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
    FDropMeshInfo ShellBullets;

	//inv
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	float SwitchTimeToWeapon = 1.0f;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	EWeaponType WeaponType = EWeaponType::Rifle;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponStats)
    int32 Round = 10;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName NameItem;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EWeaponType WeaponType = EWeaponType::Rifle;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Count;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 MaxCount;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
    GENERATED_BODY()

    ///Index Slot by Index Array
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
    UStaticMesh* WeaponStaticMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
    USkeletalMesh* WeaponSkeletalMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
    UParticleSystem* ParticleItem = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
    FTransform Offset;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
    FWeaponSlot WeaponInfo;
};

UCLASS()
class TOPDOWN_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

    public:
    UFUNCTION(BlueprintCallable)
    static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTopDownStateEffect> AddEffectClass, EPhysicalSurface SurfaceType);

    private:
    UFUNCTION()
    static bool HasEffect(AActor* TakeEffectActor, UTopDownStateEffect* TargetEffect);
};
