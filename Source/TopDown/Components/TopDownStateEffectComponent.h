// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TopDownStateEffectComponent.generated.h"


class UTopDownStateEffect;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOPDOWN_API UTopDownStateEffectComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UTopDownStateEffectComponent();

    UFUNCTION(BlueprintCallable)
    void RemoveEffect(UTopDownStateEffect* RemoveEffect);

    UFUNCTION(BlueprintCallable)
    void AddEffect(UTopDownStateEffect* NewEffect);

    UFUNCTION(BlueprintCallable)
    void SetMesh(USkeletalMeshComponent* NewEffectMesh);

    UFUNCTION(BlueprintCallable)
    TArray<UTopDownStateEffect*> GetEffects();

protected:
    UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category= Effect)
    TArray<UTopDownStateEffect*> Effects;

    UPROPERTY(ReplicatedUsing=OnRep_EffectAdd)
    UTopDownStateEffect* EffectAdd;

    UPROPERTY(ReplicatedUsing=OnRep_EffectRemove)
    UTopDownStateEffect* EffectRemove;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "Debug")
    TArray<UParticleSystemComponent*> ParticleSystemEffects;

    UPROPERTY(BlueprintReadWrite)
    USkeletalMeshComponent* EffectMesh = nullptr;

    virtual void BeginPlay() override;

    UFUNCTION()
    void OnRep_EffectAdd();

    UFUNCTION()
    void OnRep_EffectRemove();

    UFUNCTION()
    void ApplyEffect(UTopDownStateEffect* Effect);

    UFUNCTION()
    void CancelEffect(UTopDownStateEffect* Effect);

    virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
};
