// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownStateEffectComponent.h"

#include "TopDown/TopDownStateEffect.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"

UTopDownStateEffectComponent::UTopDownStateEffectComponent()
{
    PrimaryComponentTick.bCanEverTick = false;

    SetIsReplicatedByDefault(true);
}

void UTopDownStateEffectComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTopDownStateEffectComponent::RemoveEffect(UTopDownStateEffect* RemoveEffect)
{
    Effects.Remove(RemoveEffect);

    CancelEffect(RemoveEffect);
    EffectRemove = RemoveEffect;
}

void UTopDownStateEffectComponent::AddEffect(UTopDownStateEffect* NewEffect)
{
    Effects.Add(NewEffect);

    ApplyEffect(NewEffect);
    EffectAdd = NewEffect;
}

void UTopDownStateEffectComponent::SetMesh(USkeletalMeshComponent* NewEffectMesh)
{
    EffectMesh = NewEffectMesh;
}

TArray<UTopDownStateEffect*> UTopDownStateEffectComponent::GetEffects()
{
    return Effects;
}

void UTopDownStateEffectComponent::OnRep_EffectAdd()
{
    if (EffectAdd)
    {
        ApplyEffect(EffectAdd);
    }
}

void UTopDownStateEffectComponent::OnRep_EffectRemove()
{
    if (EffectRemove)
    {
        CancelEffect(EffectRemove);
    }
}

void UTopDownStateEffectComponent::ApplyEffect(UTopDownStateEffect* Effect)
{
    if (Effect && Effect->ParticleEffect)
    {
        FName NameBoneToAttached = Effect->NameBone;
        FVector Location = FVector(0);

        USceneComponent* ComponentAttached = EffectMesh;
        if (!ComponentAttached)
        {
            ComponentAttached = GetOwner()->GetRootComponent();
        }
        UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(
            Effect->ParticleEffect,
            ComponentAttached,
            NameBoneToAttached,
            Location,
            FRotator::ZeroRotator,
            FVector::OneVector,
            EAttachLocation::SnapToTarget,
            false
        );
        if (!Effect->bIsAutoDestroyParticleEffect)
        {
            ParticleSystemEffects.Add(newParticleSystem);
        }
    }
}

void UTopDownStateEffectComponent::CancelEffect(UTopDownStateEffect* Effect)
{
    if (Effect && Effect->ParticleEffect && !Effect->bIsAutoDestroyParticleEffect)
    {
        bool bIsFind = false;
        int i = 0;
        while (i < ParticleSystemEffects.Num() && !bIsFind)
        {
            if (ParticleSystemEffects[i]->Template == Effect->ParticleEffect)
            {
                bIsFind = true;
                ParticleSystemEffects[i]->DeactivateSystem();
                ParticleSystemEffects[i]->DestroyComponent();
                ParticleSystemEffects.RemoveAt(i);
            }
            i++;
        }
    }
}

bool UTopDownStateEffectComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
    check(Channel);
    check(Bunch);
    check(RepFlags);

    bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < Effects.Num(); i++)
    {
        if (Effects[i])
        {
            Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); // (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
        }
    }
    return Wrote;
}

void UTopDownStateEffectComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTopDownStateEffectComponent, Effects);
    DOREPLIFETIME(UTopDownStateEffectComponent, EffectAdd);
    DOREPLIFETIME(UTopDownStateEffectComponent, EffectRemove);
}
