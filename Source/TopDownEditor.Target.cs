// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TopDownEditorTarget : TargetRules
{
    public TopDownEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("TopDown");
    }
}